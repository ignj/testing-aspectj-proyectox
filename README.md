# README #

* Repo with a 2D Java game.

### How do I get set up? ###

* Clone the repo.
* **Build the project in eclipse to generate the bin folder**
* The external libraries are on src/lib

### Project for the subject Tecnología de Programación 2013 - Universidad Nacional del Sur ###

### Authors: ###
* Del Barrio, Ignacio
* Aguilera, Alexis
* Jócano, Ignacio