package Logica;

privileged public aspect map_loading {
	
	/**
	 * check errors after loading a map
	 */
	
	pointcut mapLoading() :
		call (String ManejadorDeMapas.FacadeLectorMapa.cargarMapa(*));

	after() returning (String map):
		mapLoading(){
			System.out.println("Advice del pointcut mapLoading [Aspecto map_loading]");
			int numOfFloors = 300;
			int numOfNewLines = 15;
			if (map.length() != (numOfFloors + numOfNewLines) )
				System.err.println("El mapa no respeta el tama�o definido. Debe ser de 15 x 20");
			for (int index = 0; index < map.length(); index++){
				if (validCharacter(map.charAt(index)) == false)
					System.err.println("El mapa contiene el caracter invalido "+map.charAt(index));				
			}
		}
	
	private boolean validCharacter(char c){
		return 	(c == 'a') ||
				(c == 's') ||
				(c == 'g') ||
				(c == 'f') ||
				(c == 'd') ||
				(c == '#') ||
				(c == 'o') ||
				(c == 'p') ||
				(c == 'c') ||
				(c == 'r') ||
				(c == 'n') ||
				(c == 'b') ||
				(c == 't') ||
				(c == 'z') ||
				(c == 'i') ||
				(c == 'O') ||
				(c == 'P') ||
				(c == 'C') ||
				(c == 'R') ||
				(c == 'N') ||
				(c == 'B') ||
				(c == 'T') ||
				(c == 'Z') ||
				(c == 'I') ||
				(c == '\n');				
	}
	
	/**
	 * checks if the loaded map is fully loaded
	 */
	
	pointcut mapInit(Logica.Mundo m) :
		call (* Logica.Mundo.initWorld()) && target(m);
	
	after (Logica.Mundo m) :
		mapInit(m){
			System.out.println("Advice del pointcut mapInit [Aspecto map_loading]");
			boolean hasEmptyBlocks = false;
			for (int indexI = 0; indexI < m.Nivel.length && !hasEmptyBlocks; indexI++)
				for (int indexJ = 0; indexJ < m.Nivel[indexI].length && !hasEmptyBlocks; indexJ++)
					if (m.Nivel[indexI][indexJ] == null) hasEmptyBlocks = true;
			if (hasEmptyBlocks){
				System.err.println("Existen casilleros del mapa sin inicializar");
			}
		}
	
}
