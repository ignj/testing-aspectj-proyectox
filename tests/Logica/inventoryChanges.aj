package Logica;

privileged public aspect inventoryChanges {

	/**
	 * fuel decrement after moving
	 */
	
	pointcut fuelDecrement(Logica.Entidades.Robot r, Logica.Entidades.Piso dest) : 
		call (boolean Logica.Entidades.Robot+.avanzar(Logica.Entidades.Piso, Logica.Entidades.Piso)) && target(r) && args(*,dest); 
	
	boolean around(Logica.Entidades.Robot r, Logica.Entidades.Piso dest) : 
		fuelDecrement(r, dest){		
			System.out.println("Advice del pointcut fuelDecrement [Aspecto inventoryChanges]");
			int oldFuel = r.getNafta();
			proceed(r, dest);
			int newFuel = r.getNafta();
			if (dest.hasRobot()){ /* the robot moved */
				assert(oldFuel > newFuel) : "La nafta no se decrementó";
				return true;
			}
			return false;
		}
	
	/**
	 * fuel, points and special action increment after getting item from floor
	 */
	
	pointcut prizeIncrement(Logica.Entidades.Robot r, Logica.Entidades.Premio prize) : 
		call (void Logica.Entidades.Objeto+.dispararEvento(Logica.Entidades.Robot)) && args(r) && target(prize);
		
	void around(Logica.Entidades.Robot r, Logica.Entidades.Premio prize) :
		prizeIncrement(r, prize){
			System.out.println("Advice del pointcut prizeIncrement [Aspecto inventoryChanges]");
			int oldFuel = r.getNafta();
			int oldPoints = r.getPuntaje();
			int oldSpecialAction = r.accionEspecial();
			proceed(r,prize);
			if (prize.getTipo().equals("nafta")){
				int newFuel = r.getNafta();
				assert(oldFuel < newFuel) : "La nafta no incremento";
			}
			if (prize.getTipo().equals("puntos")){
				int newPoints = r.getPuntaje();
				assert(oldPoints < newPoints) : "El puntaje no incremento";
			}			
			if (prize.getTipo().equals("balas")){
				int newSpecialAction = r.accionEspecial();
				assert(oldSpecialAction < newSpecialAction) : "El numero de accion especial no incremento";
			}
		}
	
	/**
	 * collectionable increment
	 */
	
	pointcut collectionableIncrement(Logica.Entidades.Robot r, Logica.Entidades.Coleccionable c) :
		call (void Logica.Entidades.Robot+.addColeccionable(Logica.Entidades.Coleccionable)) && args(c) && target(r); 
	
	void around(Logica.Entidades.Robot r, Logica.Entidades.Coleccionable c) :
		collectionableIncrement(r,c){
			System.out.println("Advice del pointcut collectionableIncrement [Aspecto inventoryChanges]");
			int oldCollectionableQuantity = r.getCantColeccionables();
			proceed(r,c);
			int newCollectionableQuantity = r.getCantColeccionables();
			assert(oldCollectionableQuantity + 1 == newCollectionableQuantity) : "El numero de colleccionables no incremento";
		}
	
	/**
	 * collectionable decrement
	 */
	
	pointcut collectionableDecrement(Logica.Entidades.Robot r, Logica.Entidades.Piso p) :
		call (void Logica.Entidades.Robot+.descargarColeccionable(Logica.Entidades.Piso)) && args(p) && target(r);
	
	void around(Logica.Entidades.Robot r, Logica.Entidades.Piso p) :
		collectionableDecrement(r,p){
			System.out.println("Advice del pointcut collectionableDecrement [Aspecto inventoryChanges]");		
			int oldCollectionableQuantity = r.getCantColeccionables();
			proceed(r,p);
			int newCollectionableQuantity = r.getCantColeccionables();
			assert(oldCollectionableQuantity - 1 == newCollectionableQuantity || oldCollectionableQuantity == 0) : "El numero de collecionables no decremento";
		}
	
	/**
	 * special action decrement
	 */
	
	pointcut specialActionDecrement(Logica.Entidades.Robot r) :
		call (void Logica.Entidades.Robot+.hazTuGracia()) && target(r);
	
	void around(Logica.Entidades.Robot r) :
		specialActionDecrement(r){
			System.out.println("Pointcut specialActionDecrement [Aspecto inventoryChanges]");
			int oldSpecialAction = r.accionEspecial();
			proceed(r);
			int newSpecialAction = r.accionEspecial();
			assert(oldSpecialAction - 1 == newSpecialAction || oldSpecialAction == 0) : "El numero de atributo para accion especial no decrementó";
		}
}
