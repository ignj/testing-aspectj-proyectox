package Logica;

public aspect objectCreation {
	
	/**
	 * Object creation logger
	 */
	
	pointcut objectCreation() :
		call(Logica.Entidades.*.new(..));
	
	after() returning (Object o):
		objectCreation(){
			System.out.println("Se crea el objeto "+o+" [Aspecto objectCreation]");
		}
}
