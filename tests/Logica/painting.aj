package Logica;

import java.util.ArrayList;

privileged public aspect painting {

	/**
	 * method to access the private field Logica.Mundo.pinceladas
	 */
	
	private ArrayList<Logica.Entidades.Pincelada> Logica.Mundo.getPinceladas(){
		return this.pinceladas;
	}
	
	/**
	 * method to access the private field Logica.Mundo.robot
	 */
	
	private Logica.Entidades.Robot Logica.Mundo.getRobot(){
		return this.robot;
	}
	
	/**
	 * method to access the field Logica.Entidades.Pincelada.piso1
	 */
	
	private Logica.Entidades.Piso Logica.Entidades.Pincelada.getPiso1(){
		return this.piso1;
	}
	
	/**
	 * method to access the field Logica.Entidades.Pincelada.piso2
	 */
	
	private Logica.Entidades.Piso Logica.Entidades.Pincelada.getPiso2(){
		return this.piso2;
	}
	
	/**
	 * method to access the field Logica.Entidades.Robot.pincel
	 */
	
	private boolean Logica.Entidades.Robot.getPincel(){
		return this.pincel;
	}
	
	/**
	 * check if the preconditions are valid when a brushstroke is made 
	 */
	
	pointcut brushstrokeAdd(Logica.Entidades.Pincelada p, Logica.Mundo m) : 
		call (boolean java.util.ArrayList.add(*)) && args(p) && this(m);
	
	boolean around(Logica.Entidades.Pincelada p, Logica.Mundo m) :
		brushstrokeAdd(p, m){
			System.out.println("Advice del pointcut brushstrokeAdd [Aspecto painting]");
			boolean isPainting = m.getRobot().getPincel();//m.getRobot().puedePintar();
			Logica.Entidades.Piso floorToPaint = p.getPiso2();
			int oldNumberOfPaintedFloors = m.getPinceladas().size();
			
			proceed(p, m);
			
			int newNumberOfPaintedFloors = m.getPinceladas().size();
			if (isPainting){
				if (oldNumberOfPaintedFloors + 1 != newNumberOfPaintedFloors)
					System.err.println("El piso no fue pintado. No se agrego la nueva pincelada a Mundo");
				if (floorToPaint != m.getRobot().getPisoActual() 
						&& (oldNumberOfPaintedFloors + 1 == newNumberOfPaintedFloors))
					System.err.println("El piso pintado es distinto al que pas� el robot");
			}
			else {//not painting
				if (oldNumberOfPaintedFloors + 1 == newNumberOfPaintedFloors)
					System.err.println("Se pinto un piso y el robot no est� habilitado para pintar");
			}
			
			return true;
		}
	
	/**
	 * check if the painted floors are deleted after cleaning
	 */
	
	pointcut clearPainting(Logica.Mundo m) :
		call (void Logica.Mundo.limpiarPinceladas()) && target(m);
	
	after(Logica.Mundo m) : 
		clearPainting(m){
			System.out.println("Advice del pointcut clearPainting [Aspecto painting]");
			if (m.getPinceladas().size() != 0){
				System.err.println("No se eliminaron las pinceladas");
			}
		}
}
