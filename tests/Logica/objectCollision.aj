package Logica;

privileged public aspect objectCollision {

	/**
	 * Check if the bullet to delete has been used
	 */
	pointcut bulletDeleting(Logica.Entidades.Bala b) : 
		call (void Logica.Mundo.eliminarDisparo(Logica.Entidades.Bala)) && args(b);
	
	public boolean Logica.Entidades.Bala.getUsed(){
		return this.used;
	}
	
	before(Logica.Entidades.Bala b) : 
		bulletDeleting(b){		
			System.out.println("Advice del pointcut bulletDeleting [Aspecto objectCollision]");
			if (b.getUsed()){
				System.out.println("La bala a eliminar ya fue utilizada");
			}
			else
				System.err.println("Se eliminará una bala que no fue marcada como usada!");
		}
	
	/**
	 * check if bullet impact damages destructible objects
	 */
	
	pointcut bulletObjectDamage() :
		call (void recibirDaño());
	
	void around(Logica.Entidades.Destructible d) : 
		bulletObjectDamage() && this(d) {
			System.out.println("Advice del pointcut bulletObjectDamage [Aspecto objectCollision]");
			int oldDurability = d.getDurabilidad();
			proceed(d);
			int newDurability = d.getDurabilidad();
			if (oldDurability == (newDurability + 1))
				System.out.println("Se decrementa la durabilidad del objeto impactado");
			else
				System.err.println("La durabilidad del destructible no se decrementa luego del impacto");
		}
	
	/**
	 * check movements of the robot and collisions with the enviroment
	 */
	
	pointcut robotMovement(Logica.Entidades.Piso orig, Logica.Entidades.Piso dest) :  
		call (boolean Logica.Entidades.Robot.avanzar(Logica.Entidades.Piso, Logica.Entidades.Piso)) && args(orig,dest);
	
	after(Logica.Entidades.Piso orig, Logica.Entidades.Piso dest) : 
		robotMovement(Logica.Entidades.Piso, Logica.Entidades.Piso) && args(orig, dest){
			System.out.println("Advice del pointcut robotMovement [Aspecto objectCollision]");
			if (!dest.esTransitable() && dest.hasRobot())
				System.err.println("El robot esta sobre un bloque no transitable");
		}
	
}
