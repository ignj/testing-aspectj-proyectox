package CrearNiveles;

privileged public aspect level_builder {
	
	/**
	 * check up movement of the pointer
	 */
	
	pointcut moveUpward(CrearNiveles.GUI g) :
		!within(level_builder) && cflow(execution (void CrearNiveles.GUI.gameLoop()) && target(g)) && if(g.upPressed) && if(!g.downPressed) && if(!g.leftPressed) && if(!g.rightPressed) ; 

	before(CrearNiveles.GUI g) :
		moveUpward(g){
			System.out.println("Advice del pointcut moveUpward [Aspecto level_builder]");
		}
	
}
