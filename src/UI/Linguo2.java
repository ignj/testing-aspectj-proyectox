package UI;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.image.AffineTransformOp;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.net.URL;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import ArchivosES.Reader;
import ArchivosES.Writer;
import Logica.AudioPlayer;
import Logica.Mundo;
import Logica.Entidades.Pincelada;
import Logica.Textures.SpriteManager;
import ManejadorDeMapas.ErrorAlCargarMapa;
import ManejadorDeMapas.FacadeLectorMapa;
import ManejadorDeMapas.leerMapaArchivo;

import java.util.Locale;
import java.util.ResourceBundle;


public abstract class Linguo2 {
	
		
	protected RenderingHints rh = new RenderingHints(
			RenderingHints.KEY_INTERPOLATION,
			RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);	
	
	protected AffineTransform at;   // the current pan and zoom transform
	protected Point2D XFormedPoint; // storage for a transformed mouse point
	
	protected double translateX;
	protected double translateY;
	protected double scale;
	protected double zoomPercent = 100;
	
	protected ImageIcon iconNew = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/new.png"));
	protected  ImageIcon iconOpen = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/open.png"));
	protected   ImageIcon iconSave = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/save.png"));
	protected    ImageIcon iconSaveAs = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/save-as.png"));
	protected   ImageIcon iconSaveCode = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/insert.png"));
    
	protected  ImageIcon iconPrint = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/document-print.png"));

	protected  ImageIcon iconOpenMap = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/world.png"));
	protected  ImageIcon iconConfig = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/settings.png"));

	protected   ImageIcon iconExit = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/exit.png"));
    
	protected  ImageIcon select = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/saveFloppy.png"));
	protected  ImageIcon freehand = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/remote.png"));
    
	protected  ImageIcon iconRobot = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/robot.png"));

    
    //Iconos de barra de Herramientas
	protected ImageIcon iconZoomIn = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/plus.png"));
	protected  ImageIcon iconZoomOut = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/minus.png"));
	protected  ImageIcon iconGrid = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/grid.png"));
	protected  ImageIcon iconStats = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/stats.png"));
	protected  ImageIcon iconColor = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/colors.png"));

	protected  ImageIcon iconReset = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/rcToStart.png"));
    protected  ImageIcon iconForward = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/rcForward.png"));
    protected   ImageIcon iconBackward = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/rcBackward.png"));
    protected  ImageIcon iconLeft = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/rcLeft.png"));
    protected  ImageIcon iconRight = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/rcRight.png"));

    protected ImageIcon iconAction = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/remote.png"));
    protected   ImageIcon iconUse = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/rcGripperPut.png"));
    protected   ImageIcon iconPaint = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/rcPaintWhite.png"));
    protected   ImageIcon iconStopPaint = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/rcStopPaint.png"));
    //ImageIcon iconAction = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/remote.png"));
    protected ImageIcon iconSound = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/sound.png"));
    protected  ImageIcon iconSoundOff = new ImageIcon(getClass().getClassLoader().getResource("Data/UI/soundOff.png"));

	protected boolean showGrid = false;
    
	protected String currentScriptPath = "";

	/** Lienzo donde se dibujan los graficos del Juego*/
	protected Canvas canvas;
	/** Lienzo donde se dibujan los graficos del HUD*/
	protected Canvas hudCanvas;
	/** El contenedor de la Ventana*/
	protected JFrame container;
	/** Panel principal donde se encuentran el Editor de Texto y el Juego*/
	protected JPanel panelPrincipal;
	/** Panel de codigo Fuente*/
	protected JTextPane codigoFuente;
	
	//Crea la fuente de sonido
	protected String directorioMusica="Data/Audio/breakingbad.ogg";
	protected AudioPlayer audioSource = new AudioPlayer(directorioMusica);
		
	public abstract void setPincelColor(Color c);
	public abstract Color getPincelColor();
	public abstract void moverRobot(int dir);
	public abstract void girarRobot(int dir);
	public abstract void resetRobot();
	public abstract void desplegarBrazoRobot();
	public abstract void activarModoPintar();
	public abstract void desactivarModoPintar();
    public abstract void gameLoop();
    public abstract void intentarAccionEspecial();
    protected abstract void reiniciarNivel();
    public abstract void cargarMapa(String levelData);
    public abstract void toggleRobot();
    
    protected ResourceBundle rb;
    protected int lang=0;
    
    protected int currentRobot=0;
    
    //Elementos que deben cambiar su etiqueta con el cambio de idioma
    private JMenu file;
    private  JMenuItem fileNew;
    private JMenuItem filePrint;
    private JMenuItem fileSave;
    private JMenuItem fileOpen;
    private  FileFilter filter1;
    private JMenuItem fileSaveAs;
    private  JMenuItem fileOpenMap;
    private JFileChooser fc = new JFileChooser();
    private JMenu fileConfig;
    private JPanel audio;
    private JButton sonido;
    private  JRadioButton civilButton =new JRadioButton("Robot Civil");
    private  JRadioButton militarButton =new JRadioButton("Robot Militar");
    private JFileChooser fd = new JFileChooser();
    private JPanel zoom ;
    private JButton zoomIn;
    private JButton zoomOut;
    private JPanel acciones;
    private JFileChooser fe = new JFileChooser();
    private JButton use;
    private  JButton paint;
    private JButton stopPaint;
    private JButton reset;
    private JButton robot;
    private JButton action;
    private JButton right;
    private JButton fwd;
    private JButton left;
    private JButton back;
    private JButton color;
    private JPanel level;
    private JPanel movimiento;
    private JMenuItem fileExit;
    private JButton grid;
    private JRadioButtonMenuItem idiomaEspa�ol;
    private JRadioButtonMenuItem idiomaIngles;
    private JRadioButtonMenuItem idiomaPirata;
    
    
    
    
    JPanel panel;
    
    public Linguo2() {
    	
    	try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
        } catch (InstantiationException ex) {
        } catch (IllegalAccessException ex) {
        } catch (UnsupportedLookAndFeelException ex) {
        }
    	
    	container = new JFrame("Linguo 2");
    	panelPrincipal = (JPanel) container.getContentPane();
    	canvas = new Canvas();
    	canvas.setVisible(true);
    	
    	translateX = 0;
	    translateY = 0;
	    scale = 1;
	    
	    PanningHandler panner = new PanningHandler();
		canvas.addMouseListener(panner);
		canvas.addMouseMotionListener(panner);
		canvas.addMouseWheelListener(panner);
    	
    	
    	
    	//**********************
    	//Configuracion de la Ventana
    	//**********************
    	container.setSize(1024, 760);
    	container.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	container.setLocationRelativeTo(null);

    	// Resolucion de la Ventana
    	panelPrincipal.setPreferredSize(new Dimension(1024,760));
    	container.setExtendedState(JFrame.MAXIMIZED_BOTH);
    	init();
        initUI();
        
        // hacer la ventana visible
     	container.pack();
     	container.setResizable(true);
     	container.setVisible(true);
     	
     	// Agrega un oyente para responder al cierre de la ventana
     	container.addWindowListener(new WindowAdapter() {
     		public void windowClosing(WindowEvent e) {
     			System.exit(0);
     		}
     	});
     	
     		
    }
    
    /**
	 * Inicializador de las variables Locale y resourceBundle
	 */
    
    private void init(){
    	Locale currentLocale = crearLocale(0);
		rb = ResourceBundle.getBundle("UI.Etiquetas", currentLocale);
    }
    /**
	 * M�todo que devuelve un Locale dependiendo del indice de idioma que se le entregue
	 * @param idioma Indice del idioma
	 * @return Locale ya configurado con el idioma
	 * @author C�tedra T�cnolog�a de la Programaci�n
	 */
    
    protected Locale crearLocale(int idioma){
    	Locale aux;
		
		switch (idioma){ //Si el indice pasado es:
		case 1: //1, hablamos ingl�s de USA.
			aux= new Locale("en","US");
			break;
		/*case 2: //2, hablamos de alemán de Alemania
			aux= new Locale("de","DE");
			break;
		case 3: //3, hablamos de Frances de Belgica
			aux= new Locale("fre","FR");
			break;*/
		case 2: //2, hablamos de ingl�s Pirata 
			aux= new Locale("en","PI"); 
			break;
		default:
			aux=new Locale.Builder().build();
			break;
		}
		return aux;
	}
    	
    

    public final void initUI() {
    	  
    	//**********************
    	//BARRA DE MENU PRINCIPAL
    	//**********************
        JMenuBar menubar = new JMenuBar();

         file = new JMenu(rb.getString("Archivo"));
        
        fileNew = new JMenuItem(rb.getString("Nuevo"), iconNew);
        
        fileNew.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	currentScriptPath = "";
            	codigoFuente.setText("");
            	reiniciarNivel();
            }

        });

      fileOpen = new JMenuItem("Abrir", iconOpen);
        
        fileOpen.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	importarCodigoFuente();
            }

        });

        fileSave = new JMenuItem("Guardar" , iconSave);

        fileSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	exportarCodigoFuente();
            }

        });

        fileOpenMap = new JMenuItem("Abrir Mapa", iconOpenMap);
        
        fileOpenMap.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	//fc = new JFileChooser(); 
                fc.setDialogTitle("Abrir Mapa");
                fc.setApproveButtonText("Open");

                filter1 = new FileNameExtensionFilter("Linguo Map *.map", "map");
                fc.addChoosableFileFilter(filter1);

                fc.setFileSelectionMode(JFileChooser.FILES_ONLY); 
                int returnVal = fc.showOpenDialog(container); 

                if (returnVal == JFileChooser.APPROVE_OPTION) { 
                    FacadeLectorMapa Lector= new leerMapaArchivo();
                    String levelData = "";
                    try {
                    	levelData = Lector.cargarMapa(fc.getSelectedFile().getAbsolutePath());
                    	cargarMapa(levelData);
                    	currentScriptPath = " - "+fc.getSelectedFile().getAbsolutePath();
            		} catch (ErrorAlCargarMapa e) {			
            			e.printStackTrace();
            		}
                }
            }

        });
        
        fileConfig = new JMenu("Idioma");
        //fileSave.setMnemonic(KeyEvent.VK_P);
        
        
       //a group of radio button menu items
        //menu.addSeparator();
        ButtonGroup group = new ButtonGroup();
        idiomaEspa�ol = new JRadioButtonMenuItem("Espa�ol");
        idiomaEspa�ol.setSelected(true);
        idiomaEspa�ol.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	//Se crea el nuevo Locale
     			Locale currentLocale = crearLocale(0); //Creamos el locale correspondiente
     			//Asociamos el archivo .propierties
     			rb = ResourceBundle.getBundle("UI.Etiquetas", currentLocale);
     			
     			cambiarIdioma();
            }

        });
        //rbMenuItem.setMnemonic(KeyEvent.VK_R);
        group.add(idiomaEspa�ol);
        fileConfig.add(idiomaEspa�ol);
        
        idiomaIngles = new JRadioButtonMenuItem("Ingl�s");
        idiomaIngles.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	//Se crea el nuevo Locale
     			Locale currentLocale = crearLocale(1); //Creamos el locale correspondiente
     			//Asociamos el archivo .propierties
     			rb = ResourceBundle.getBundle("UI.Etiquetas", currentLocale);
     			
     			cambiarIdioma();
            }

        });
        //idiomaIngles.setSelected(false);
        //rbMenuItem.setMnemonic(KeyEvent.VK_R);
        group.add(idiomaIngles);
        fileConfig.add(idiomaIngles);
        
        idiomaPirata = new JRadioButtonMenuItem("Ingles (Pirata)");
        idiomaPirata.setSelected(true);
        idiomaPirata.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	//Se crea el nuevo Locale
     			Locale currentLocale = crearLocale(2); //Creamos el locale correspondiente
     			//Asociamos el archivo .propierties
     			rb = ResourceBundle.getBundle("UI.Etiquetas", currentLocale);
     			
     			cambiarIdioma();
            }

        });
        //rbMenuItem.setMnemonic(KeyEvent.VK_R);
        group.add(idiomaPirata);
        fileConfig.add(idiomaPirata);

        fileExit = new JMenuItem("Salir", iconExit);
        //fileExit.setMnemonic(KeyEvent.VK_C);
        fileExit.setToolTipText("Salir de Linguo2");
        fileExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, ActionEvent.CTRL_MASK));

        fileExit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                System.exit(0);
            }

        });

        file.add(fileNew);
        file.add(fileOpen);
        file.add(fileSave);

        file.addSeparator();
        file.add(fileOpenMap);
        file.add(fileConfig);
        file.addSeparator();
        file.add(fileExit);

        menubar.add(file);

        container.setJMenuBar(menubar);
        //**********************
    	//FIN BARRA DE MENU PRINCIPAL
    	//**********************

        //**********************
    	//BARRA DE HERRAMIENTAS
    	//**********************
        //JTabbedPane tabbedPane = new JTabbedPane();
        //Panel de Visual
        JPanel viewPanel = new JPanel(false);
        viewPanel.setLayout(new BoxLayout(viewPanel, BoxLayout.X_AXIS));
        viewPanel.add(createAudioPanel());
        viewPanel.add(createZoomPanel());
        viewPanel.add(createGridPanel());

        viewPanel.add(createMovementPanel());
        viewPanel.add(createAccionPanel());

        
        
        JToolBar toolbar = new JToolBar();
        toolbar.setFloatable(false);

        JButton bexit = new JButton(iconExit);
        bexit.setBorder(new EmptyBorder(0 ,0, 0, 0));
        toolbar.add(bexit);

        container.add(viewPanel, BorderLayout.NORTH);
        //**********************
    	//FIN BARRA DE HERRAMIENTAS
    	//**********************
       
        //**********************
    	//PANEL PRINCIPAL DEL JUEGO
    	//**********************
        
        //Panel de Codigo Fuente
        JScrollPane scrollPane = new JScrollPane();
        codigoFuente = new JTextPane();
        codigoFuente.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));
        //Clase de Tercero para agregar Numeros de Linea
        TextLineNumber tln = new TextLineNumber(codigoFuente);
        scrollPane.setRowHeaderView(tln);
        
        scrollPane.getViewport().add(codigoFuente);
        
        //canvasViewport.getViewport().add(canvas);
        panelPrincipal.add(scrollPane,BorderLayout.CENTER);
        panelPrincipal.add(canvas,BorderLayout.EAST); //Panel de Graficos del Juego

        //**********************
    	//FIN PRINCIPAL PANEL DEL JUEGO
    	//**********************
        
        //**********************
    	//BARRA DE ESTADO
    	//**********************

        JLabel statusbar = new JLabel(" ");
        statusbar.setPreferredSize(new Dimension(-1, 45));
        statusbar.setBorder(LineBorder.createGrayLineBorder());
        container.add(statusbar, BorderLayout.SOUTH);
        //**********************
    	//FIN BARRA DE ESTADO
    	//**********************
        
    }
    
    protected JPanel createAudioPanel() {
    	 audio = makeTextPanel("Audio");
        
        JToolBar zoomToolbar = new JToolBar();
        zoomToolbar.setLayout(new GridBagLayout());
        zoomToolbar.setFloatable(false);

        sonido = getBoton("M�sica", iconSound);
        sonido.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	if(sonido.getIcon() == iconSound) {
            		sonido.setIcon(iconSoundOff);
            		audioSource.stopMusic(0);
            	}
            	else{
            		sonido.setIcon(iconSound);
            		audioSource.startMusic(0);
            	}
            }
        });
        zoomToolbar.add(sonido);

        audio.add(zoomToolbar);
        
        return audio;
    }
    
    protected JPanel createZoomPanel() {
    	 zoom = makeTextPanel("Zoom");
        
        JToolBar zoomToolbar = new JToolBar();
        zoomToolbar.setLayout(new GridBagLayout());
        zoomToolbar.setFloatable(false);

        zoomIn = getBoton("Zoom In", iconZoomIn);
        zoomIn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	zoomPercent += 10;
            	scale = Math.max(0.00001, zoomPercent / 100.0);
            }
        });
        zoomToolbar.add(zoomIn);
        
        zoomToolbar.addSeparator();
        
        zoomOut = getBoton("Zoom Out", iconZoomOut);
        zoomOut.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	zoomPercent -= 10;
            	scale = Math.max(0.00001, zoomPercent / 100.0);
            }
        });
        zoomToolbar.add(zoomOut);

        zoom.add(zoomToolbar);
        
        return zoom;
    }
    
    protected JPanel createGridPanel() {
    	level = makeTextPanel("Nivel");
        
        JToolBar Toolbar = new JToolBar();
        Toolbar.setFloatable(false);
        Toolbar.setLayout(new GridBagLayout());

        grid = getBoton("Ver Grilla", iconGrid);
        grid.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	showGrid = !showGrid;
            }
        });
        Toolbar.add(grid);

        Toolbar.addSeparator();
        
        color = getBoton("Color Pincel", iconColor);
        color.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	JColorChooser clr = new JColorChooser();
                Color color = clr.showDialog(new JPanel(), "Choose Color", getPincelColor());
                setPincelColor(color);
            }
        });
        Toolbar.add(color);

        level.add(Toolbar);
        
        return level;
    }
    
    protected JPanel createMovementPanel() {
    	movimiento = makeTextPanel("Movimiento");
        
        JToolBar Toolbar = new JToolBar();
        Toolbar.setFloatable(false);
        Toolbar.setLayout(new GridBagLayout());
        
        reset = getBoton("Resetear", iconReset);
        reset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	resetRobot();
            }
        });
        Toolbar.add(reset);
        
        Toolbar.addSeparator();
        
        fwd = getBoton("Adelante", iconForward);
        fwd.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	moverRobot(1);
            }
        });
        Toolbar.add(fwd);
        
        Toolbar.addSeparator();
        
        back = getBoton("Atr�s", iconBackward);
        back.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	moverRobot(-1);
            }
        });
        Toolbar.add(back);
        
        Toolbar.addSeparator();
        
        left = getBoton("Izquierda", iconLeft);
        left.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	girarRobot(-1);
            }
        });
        Toolbar.add(left);
        
        Toolbar.addSeparator();
        
        right = getBoton("Derecha", iconRight);
        right.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	girarRobot(1);
            }
        });
        Toolbar.add(right);

        movimiento.add(Toolbar);
        
        return movimiento;
    }
    
    protected JPanel createAccionPanel() {
    	acciones = makeTextPanel("Acciones");
        
        JToolBar Toolbar = new JToolBar();
        Toolbar.setFloatable(false);
        Toolbar.setLayout(new GridBagLayout());
        
        action = getBoton("Acci�n", iconAction);
        action.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	intentarAccionEspecial();
            }
        });
        Toolbar.add(action);
        
        Toolbar.addSeparator();
        
        use = getBoton("Brazo", iconUse);
        use.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	desplegarBrazoRobot();
            }
        });
        Toolbar.add(use);
        
        Toolbar.addSeparator();
        
        paint = getBoton("Pintar", iconPaint);
        paint.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	activarModoPintar();
            }
        });
        Toolbar.add(paint);
        
        Toolbar.addSeparator();
        
        stopPaint = getBoton("Normal", iconStopPaint);
        stopPaint.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	desactivarModoPintar();
            }
        });
        Toolbar.add(stopPaint);
        
        Toolbar.addSeparator();
        
        robot = getBoton("Robot", iconRobot);
        robot.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	JDialog ad = new JDialog();
            	ad.setTitle("Seleccionar Robot");
            	ad.setContentPane(selectRobot());
            	ad.pack();
                ad.setVisible(true);
                ad.setLocationRelativeTo(null);
            }
        });
        Toolbar.add(robot);

        acciones.add(Toolbar);
        
        return acciones;
    }
    
    protected JPanel makeTextPanel(String text) {
        JPanel panel = new JPanel(false);
        
        JLabel panelName = new JLabel(text);
        panelName.setPreferredSize(new Dimension(-1, 20));
        panelName.setHorizontalAlignment(JLabel.CENTER);
        panelName.setBorder(LineBorder.createGrayLineBorder());
        
        //JLabel filler = new JLabel(text);
        //filler.setHorizontalAlignment(JLabel.CENTER);
        panel.setLayout(new BorderLayout());
        panel.add(panelName, BorderLayout.SOUTH);
        return panel;
    }
    
  /**
   * se encarga de Guardar el estado del contenido del panel de control
   * 
   * @return un boolean indicando si la operaci�n se realiz� con �xito
   */
    public boolean exportarCodigoFuente(){ 
        //fd = new JFileChooser(); 
        fd.setDialogTitle("Guardar Script");
        fd.setApproveButtonText("Save");

        FileFilter filter1 = new FileNameExtensionFilter("Linguo Script *.ls", "ls");
        fd.addChoosableFileFilter(filter1);
        
        fd.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES); 
        int returnVal = fd.showOpenDialog(container); 
        if (returnVal == JFileChooser.APPROVE_OPTION) {     
            Writer escritor =  new Writer(fc.getSelectedFile().getAbsolutePath()); 
            String cadena = codigoFuente.getText();
            if(cadena != null){ 
                escritor.write(cadena); 
            } 
            escritor.cerrarArchivo(); 
            currentScriptPath = " - "+fd.getSelectedFile().getAbsolutePath();
            return true;
        }
        return false;
    }
    /**
     * Abre una partida guardada en la ruta que indique el usuario
     */
    public void importarCodigoFuente(){ 
        //fe = new JFileChooser(); 
        fe.setDialogTitle("Abrir Script");
        fe.setApproveButtonText("Open");

        FileFilter filter1 = new FileNameExtensionFilter("Linguo Script *.ls", "ls");
        fe.addChoosableFileFilter(filter1);

        fe.setFileSelectionMode(JFileChooser.FILES_ONLY); 
        int returnVal = fe.showOpenDialog(container); 

        if (returnVal == JFileChooser.APPROVE_OPTION) { 
            Reader lector = new Reader(fc.getSelectedFile().getAbsolutePath()); 

            String data = lector.readln(); 
            codigoFuente.setText(data);
            //loadData(data);

            lector.cerrarArchivo(); 
            currentScriptPath = " - "+fe.getSelectedFile().getAbsolutePath();
        }
    } 
    
    public JButton getBoton(String label, ImageIcon ico) {
    	
    	JButton zoomIn = new JButton(label,ico);
        zoomIn.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));//(new EmptyBorder(0 ,0, 0, 0));
        zoomIn.setVerticalTextPosition(AbstractButton.BOTTOM);
        zoomIn.setHorizontalTextPosition(AbstractButton.CENTER);
    	return zoomIn;
    }
    /**
     * Crea un Panel donde se puede elegir el robot que se quiera utilizar
     * @return Panel con los robots a elegir
     */
    public JPanel selectRobot() {
    	JPanel pan = new JPanel(false);
    	//Create the radio buttons.
        //civilButton = new JRadioButton("Robot Civil");
        civilButton.setActionCommand("Robot Civil");
        civilButton.setSelected(currentRobot == 0);

        //militarButton = new JRadioButton("Robot Militar");
        militarButton.setActionCommand("Robot Militar");
        militarButton.setSelected(currentRobot == 1);

        //Group the radio buttons.
        ButtonGroup group = new ButtonGroup();
        group.add(civilButton);
        group.add(militarButton);

        //Register a listener for the radio buttons.
        civilButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	if(currentRobot != 0) {
            	toggleRobot();
            	currentRobot=0;
            	}
            }
        });
        militarButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
            	if(currentRobot != 1) {
            	toggleRobot();
            	currentRobot=1;
            	}
            }
        });
        
        String url = "Data/Textures/"+"Default"+"/"+"hudMilitar.png";
        
        JLabel picture = new JLabel(createImageIcon(url));
        
      //Put the radio buttons in a column in a panel.
        JPanel radioPanel = new JPanel(new GridLayout(0, 1));
        radioPanel.add(civilButton);
        radioPanel.add(militarButton);


        pan.add(radioPanel, BorderLayout.LINE_START);
        pan.add(picture, BorderLayout.CENTER);
        pan.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
        
        return pan;
    }
    
    protected ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = this.getClass().getClassLoader().getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }
    

    
    private void cambiarIdioma(){
    	file.setText(rb.getString("Archivo"));
    	   
    	 fileNew.setText(rb.getString("Nuevo"));
    	 //filePrint.setText(rb.getString("Imprimir"));
    	 fileSave.setText(rb.getString("Guardar"));
    	 fileOpen.setText(rb.getString("Abrir"));
    	 //fileSaveAs.setText(rb.getString("Guardar_Como"));
    	 fileOpenMap.setText(rb.getString("Abrir_Mapa"));
    	 fc.setName(rb.getString("Abrir_Mapa"));
    	 fileConfig.setText(rb.getString("Idioma"));
    	 audio.setName(rb.getString("Audio"));
    	 sonido.setText(rb.getString("Musica"));
    	 civilButton.setText(rb.getString("Robot_Civil"));
    	 militarButton.setText(rb.getString("Robot_Militar"));
    	 fd.setDialogTitle(rb.getString("Guardar_Script"));
    	 zoom.setName(rb.getString("Zoom"));
    	 zoomIn.setText(rb.getString("Zoom_In"));
    	 zoomOut.setText(rb.getString("Zoom_Out"));
    	 acciones.setName(rb.getString("Acciones"));
    	 fe.setName(rb.getString("Abrir_Script"));
    	 use.setText(rb.getString("Brazo"));
    	 paint.setText(rb.getString("Pintar"));
    	 stopPaint.setText(rb.getString("Normal"));
    	 reset.setText(rb.getString("Resetear"));
    	 robot.setText(rb.getString("Robot"));
    	 action.setText(rb.getString("Accion"));
    	 right.setText(rb.getString("Derecha"));
    	 fwd.setText(rb.getString("Adelante"));
    	 left.setText(rb.getString("Izquierda"));
    	 back.setText(rb.getString("Atras"));
    	 color.setText(rb.getString("Color"));
    	 level.setName(rb.getString("Nivel"));
    	 movimiento.setName(rb.getString("Movimiento"));
    	 fileExit.setText(rb.getString("Salir"));
    	 grid.setText(rb.getString("Ver_Grilla"));
    	 idiomaEspa�ol.setText(rb.getString("Espa�ol")); 
    	 idiomaIngles.setText(rb.getString("Ingles"));
    	 idiomaPirata.setText(rb.getString("Ingles_Pirata"));
    	
    	
    }
    
    
    
    public Dimension getPreferredSize() {
	    return new Dimension(800, 600);
	}
	class PanningHandler implements MouseListener,
	MouseMotionListener, MouseWheelListener {
	double referenceX;
	double referenceY;
	// saves the initial transform at the beginning of the pan interaction
	AffineTransform initialTransform;

	// capture the starting point 
	public void mousePressed(MouseEvent e) {

	// first transform the mouse point to the pan and zoom
	// coordinates
	try {
	XFormedPoint = at.inverseTransform(e.getPoint(), null);
	}
	catch (NoninvertibleTransformException te) {
	System.out.println(te);
	}

	// save the transformed starting point and the initial
	// transform
	referenceX = XFormedPoint.getX();
	referenceY = XFormedPoint.getY();
	initialTransform = at;
	}

	public void mouseDragged(MouseEvent e) {

	// first transform the mouse point to the pan and zoom
	// coordinates. We must take care to transform by the
	// initial tranform, not the updated transform, so that
	// both the initial reference point and all subsequent
	// reference points are measured against the same origin.
	try {
	XFormedPoint = initialTransform.inverseTransform(e.getPoint(), null);
	}
	catch (NoninvertibleTransformException te) {
	System.out.println(te);
	}

	// the size of the pan translations 
	// are defined by the current mouse location subtracted
	// from the reference location
	double deltaX = XFormedPoint.getX() - referenceX;
	double deltaY = XFormedPoint.getY() - referenceY;

	// make the reference point be the new mouse point. 
	referenceX = XFormedPoint.getX();
	referenceY = XFormedPoint.getY();

	translateX += deltaX;
	translateY += deltaY;

	// schedule a repaint.
	//canvas.repaint();
	}

	public void mouseClicked(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
	public void mouseMoved(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}
	
	public void mouseWheelMoved(MouseWheelEvent e) {
	       String message;
	       int notches = e.getWheelRotation();
	       if (notches < 0) {//UP
	    	   zoomPercent += 10;
          		scale = Math.max(0.00001, zoomPercent / 100.0);
	       } else {//DOWN
	    	   zoomPercent -= 10;
           		scale = Math.max(0.00001, zoomPercent / 100.0);
	       }/*
	       if (e.getScrollType() == MouseWheelEvent.WHEEL_UNIT_SCROLL) {
	       } else { //scroll type == MouseWheelEvent.WHEEL_BLOCK_SCROLL
	       }*/
	       
	    }
	}

}
