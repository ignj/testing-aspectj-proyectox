package ManejadorDeMapas;
import java.io.*;

public class leerMapaArchivo extends FacadeLectorMapa {
	
	/**
	 * Crea un nuevo objeto del cual leer mapas
	 */
	public leerMapaArchivo(){}
		
	/**
	 * Dada una direccion de un archivo de texto devuelve el contenido de dicho archivo.
	 * @return contenido del archivo de texto (string del mapa)
	 */
	public String cargarMapa(String direccion) throws ErrorAlCargarMapa {
		File archivo=null;
		FileReader lectorArchivo=null;
		BufferedReader lectorBuffer=null;
		String stringMapa="";
		String linea;
		
		try{
			archivo= new File(direccion);
			lectorArchivo= new FileReader(archivo);
			lectorBuffer = new BufferedReader(lectorArchivo);		
			
			while((linea=lectorBuffer.readLine())!=null)
	            stringMapa+=linea+"\n";
			
			return stringMapa;
		}
		catch (IOException e){
			throw new ErrorAlCargarMapa(e.getMessage());
		}
		finally{
			try{
				if (lectorBuffer!=null)
					lectorBuffer.close();
			}
			catch (IOException e){
				throw new ErrorAlCargarMapa(e.getMessage());
			}
		}
		
	}
}		

