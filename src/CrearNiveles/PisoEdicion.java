package CrearNiveles;


import Logica.Textures.Sprite;
import Logica.Textures.SpriteManager;

import java.awt.Color;
import java.awt.Graphics;


	/**
	 * Entidad que representa un Piso del juego.		 
	 * @author Ignacio del Barrio, Ignacio Jocano, Alexis Aguilera
	 * 
	 */
	public class PisoEdicion{
		
		/** Posicion actual en x */  
		protected double x;
		/** Posicion actual en y */
		protected double y;
		/** El Sprite que representa a esta Entidad */
		protected Sprite sprite;
		/** nombre de la imagen que el sprite representa*/
		protected String nombre;
		/** Tipo del Piso (Arena, Asfalto, Objetivo) */
		protected char tipo;
		/** Color del que esta pintado el Piso */
		//protected Color color;
		protected PisoEdicion objeto;
		/** Sprite del Objetivo */
		protected Sprite objetivo;
		
		/** verdadero si tiene bordes*/
		protected boolean tieneBorde=false;
		
		
		//CONSTRUCTORES
		
		/**
		 * crea un nuevo pisoEdicion
		 * @param ref nombre del sprite en disco
		 * @param x posicion en eje x
		 * @param y posicion en eje y
		 * @param tipo letra que corresponde al tipo de piso que es
		 */
		public PisoEdicion(String ref, double x,double y, char tipo) {
			this.sprite = SpriteManager.get().getSprite(ref);
			this.x = x;
			this.y = y;
			objetivo = SpriteManager.get().getSprite("objetivo.png");
			this.tipo = tipo;
			nombre=ref;
			//color = null;
		}
		
		/**
		 * crea un nuevo pisoEdicion que tiene un objeto encima
		 * @param ref nombre del sprite en disco
		 * @param x posicion en el eje x
		 * @param y posicion en el eje y
		 * @param tipo letra que corresponde al tipo de piso que es
		 * @param nombreObjeto nombre del sprite del objeto que contiene este piso
		 */
		public PisoEdicion(String ref, double x,double y, char tipo, String nombreObjeto) {
			this.sprite = SpriteManager.get().getSprite(ref);
			this.x = x;
			this.y = y;			

			objetivo = SpriteManager.get().getSprite("objetivo.png");	
			objeto = new PisoEdicion(nombreObjeto,x,y);
			this.tipo = tipo;
			nombre=ref;
		//	color = new Color(0);
		}
		
		/**
		 * crea un nuevo pisoEdicion
		 * @param ref nombre del sprite en disco
		 * @param x posicion en el eje x
		 * @param y posicion en el eje y
		 */
		public PisoEdicion(String ref, double x, double y) {
			this.sprite = SpriteManager.get().getSprite(ref);
			this.x = x;
			this.y = y;
			nombre=ref;
			objetivo = SpriteManager.get().getSprite("objetivo.png");
		//	color = new Color(0);
		}
		
		
		/**
		 * crea un nuevo PisoEdicion
		 * @param ref nombre del sprite
		 * @param tipo letra que representa al piso actual
		 */
		public PisoEdicion(String ref, char tipo){
			this.sprite = SpriteManager.get().getSprite(ref);
			nombre=ref;
			this.tipo = tipo;			
		}
		
		///////////////////////////////////////FIN CONSTRUCTORES//////////////////////
		
		/** 
		 * Retorna el tipo del Piso
		 * @returns tipo Tipo del Piso
		 */
		public char getTipo() {
			return tipo;
		}
		
		/**
		 * modifica el tipo del piso
		 * @param c tipo de piso
		 */
		public void setTipo(char c){
			tipo=c;
		}
		
		/** 
		 * Actualiza la referencia al Objeto en el Piso
		 * @param o Objeto
		 */
		public void setObjeto(PisoEdicion o) {
			objeto = o;
		}

		
		/**
		 * Redefinicion del metodo Draw para poder Dibujar tambien los objetos en el Piso.
		 * @param g El contexto Grafico en el cual dibujar
		 */
		public void draw(Graphics g) {
			sprite.draw(g,(int) x,(int) y);
			//if(tipo == 'o'){
				//objetivo.draw(g, (int) x,(int) y);
			//}
			if (objeto != null){				
				objeto.draw(g);
			}
			if (tieneBorde){
				g.drawRect((int)x, (int)y, 40, 40);
				g.drawRect((int)x+1, (int)y+1, 38, 38);
			}
		}
	
		/** 
		 * Retorna el Objeto actual que esta en este Piso
		 * @returns objeto Objeto en este Piso
		 */
		public PisoEdicion getObjeto() {
			return objeto;
		}
		
		/**
		 * modifica la posicion del piso en el eje X
		 * @param x posicion que se le asigna al piso
		 */
		public void setX(double x){
			if (objeto!=null){
				this.x=x;
				objeto.setX(x);
			}
			else this.x=x;
		}
		
		/**
		 * modifica la posicion del piso en el eje Y
		 * @param y posicion que se le asigna al piso
		 */
		public void setY(double y){
			if (objeto!=null){
				this.y=y;
				objeto.setY(y);
			}
			else this.y=y;
		}
		
		/**
		 * devuelve el valor de la posicion del piso en el eje Y
		 * @return valor de la posicion del piso
		 */
		public double getY(){
			return y;
		}
		
		/**
		 * devuelve el valor de la posicion del piso en el eje X
		 * @return 
		 */
		public double getX(){
			return x;
		}
		
		/**
		 * devuelve el nombre del sprite
		 * @return
		 */
		public String getNombreSprite(){
			return nombre;
		}
		
		/**
		 * setea el nombre del sprite
		 * @param s
		 */
		public void setNombreSprite(String s){
			nombre=s;
		}
		
		/**
		 * Clona superficialmente un piso
		 */
		
		public PisoEdicion clone(){
			return new PisoEdicion(nombre,tipo);
		}
		
		/**
		 * clona un piso de forma profunda
		 * @return clon del piso
		 */
		
		public PisoEdicion cloneProfundo(){
			if (objeto==null)
				return new PisoEdicion(nombre,x,y,tipo);
			else
				return new PisoEdicion(nombre,x,y,tipo,objeto.getNombreSprite());
		}
		
		public void ponerBorde(){
			tieneBorde=true;
		}
}
