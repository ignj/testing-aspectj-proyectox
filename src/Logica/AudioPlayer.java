package Logica;
import java.io.IOException;

import org.newdawn.easyogg.OggClip;

/**
 * Clase manejadora del Audio
 * 
 */
public class AudioPlayer {

	private OggClip[] sounds;
	
	public AudioPlayer(String nombreArchivo) {
		sounds = new OggClip[1];
		try {
			sounds[0] = new OggClip(this.getClass().getClassLoader().getResourceAsStream(nombreArchivo));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Inicia el clip i
	 * 
	 */
	public void startMusic( int i) {
		sounds[i].stop();
		sounds[i].loop();
	}
	
	/**
	 * detiene el clip i
	 * @param i clip a detener
	 */
	public void stopMusic(int i){
		sounds[i].stop();
	}
	
	/**
	 * verifica si un clip esta detenido
	 * @param i clip a chequear
	 * @return verdadero si el clip esta detenido
	 */
	public boolean isStopped(int i){
		return sounds[i].stopped();
	}
}
