package Logica.Textures;

/**
 * Skin por Defecto del juego.
 * 
 * @author Ignacio del Barrio, Ignacio Jocano, Alexis Aguilera
 */
public class DefaultSkin extends SkinManager{

	/**
	 * Constructor por Defecto. El nombre del skin representa la carpeta
	 * en la cual se guardan las texturas.
	 * Data/Textures/NOMBRE
	 */
	public DefaultSkin() {
		super("Default");
	}

}
