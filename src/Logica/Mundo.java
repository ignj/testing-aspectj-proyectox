package Logica;



import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import Logica.Entidades.Bala;
import Logica.Entidades.Destructible;
import Logica.Entidades.Entidad;
import Logica.Entidades.Militar;
import Logica.Entidades.Objeto;
import Logica.Entidades.Pincelada;
import Logica.Entidades.Piso;
import Logica.Entidades.Robot;
import UI.Linguo2;

/**
 * Punto de acceso al Juego. Esta clase actua tanto como el 
 * Adminstrador de la logica del juego como el encargado de 
 * indicar que se debe dibujar en pantalla.
 * 
 * El dibujado se logra atravez de un ciclo infinito que comprueba
 * el estado de las Entidades graficas y les pide que se dibujen de
 * ser nesesario.
 * Contiene una clase anidada que controla el ingreso de comandos
 * por teclado del Usuario.
 * 
 * Como Administrador, sera informado sobre los eventos
 * del juego y el usuario. Tomara las acciones nesesarias en respuesta
 * a estos eventos.
 * 
 * @author Ignacio del Barrio, Ignacio Jocano, Alexis Aguilera
 * 
 */
@SuppressWarnings("serial")
public class Mundo extends Linguo2{
	
	/** Direccion en la que el robot hace un movimiento*/
	private int directionMove;
	/** Direccion en la que el robot gira*/
	private int direccionGiro;
	
	private ArrayList<Pincelada> pinceladas = new ArrayList<Pincelada>();
	
	/** Distancia del borde la ventana en la que empieza a dibujar */
	private final int OFFSET = 0;//30;
	/** Espacio entre los bloques que componen el nivel */
    private final int SPACE = 40;//20;
    /** Verdadero si el juego esta iniciando */
    private boolean startUp = true;
    /** Referencia al HUD del juego */
    private HUD hud;
    /** Verdadero si el HUD esta visible */
	private boolean toggleHUD = false;

	/** Verdadero si la Ayuda esta visible */
	private boolean toggleHelp = false;
	/** Tipo actual del Robot (Civil - Militar) */
    private String tipoRobot = "Civil";
    /** Referencia a la Fuente de Audio principal */
    //private AudioPlayer audioSource;    
	/** Strategy que utiliza el Buffer para hacer uso de Accelerated Page Flipping */
	private BufferStrategy strategy;
	/** Verdadero si el juego esta corriendo */
	private boolean gameRunning = true;
	/** Coleccion de Balas instanciadas */
	private ArrayList<Bala> balas = new ArrayList<Bala>();
	/** Coleccion de Balas que deben ser eliminadas en este ciclo */
	private ArrayList<Bala> balasBasura = new ArrayList<Bala>();
	/** La entidad que representa al jugador */
	private Robot robot;
	/** Posicion en X del Robot en la Matriz del Nivel (FILA) */
	private int robotPosFil;
	/** Posicion en Y del Robot en la Matriz del Nivel  (COLUMNA)*/
	private int robotPosCol;
	/** Tiempo transcurrido desde la ultima accion */
	private long ultimaAccion = 0;
	/** Intervalo de tiempo antes de ejecutar una Accion (ms) */
	private long intervaloDeAccion = 250;
	/** Mensaje a mostrar mientras se espera el ingreso de una tecla */
	private String message = "";
	/** Verdadero si el juego esta detenido hasta el ingreso de una tecla */
	private boolean waitingForKeyPress = false;
	/** Verdadero si la tecla ARRIBA esta siendo presionada */
	private boolean upPressed = false;
	/** Verdadero si la tecla ABAJO esta siendo presionada */
	private boolean downPressed = false;
	/** Verdadero si la tecla IZQUIERDA esta siendo presionada */
	private boolean leftPressed = false;
	/** Verdadero si la tecla DERECHA esta siendo presionada */
	private boolean rightPressed = false;
	/** Verdadero si la tecla DISPARO esta siendo presionada */
	private boolean firePressed = false;
	/** Verdadero si estamos cambiando el Tipo de Robot*/
	private boolean changeRobot = false;
	/** Verdadero si estamos desplegando el Brazo Robot */
	private boolean brazoPressed = false;
	/** Verdadero si hay que ejecutar logica del juego en este ciclo, como resultado de un evento */
	//private boolean logicRequiredThisLoop = false;
	/** La ultima vez que calculamos el fps */
	private long lastFpsTime;
	/** Numero actual de cuadros por segundo */
	private int fps;
	/** Nombre de la ventana */
	private String windowTitle = "Linguo 2";
	/** Matriz que representa el Nivel actual */
	protected Piso[][] Nivel;
	/** El Piso Objetivo del juego */
	protected Piso pisoObjetivo;    
    /** Datos del Nivel de prueba */
	private String levelData =
		    "dddddddddddddddddddd\n"
		  + "daaaaaaaaaaaadpdgdid\n"
          + "daaaaaaaapaaadddddad\n"
          + "daadddaaaaaaaaaaaaad\n"
          + "daadgddaaaaaaaaaaaad\n"
          + "daaddgdaaaaaaaaoaaad\n"
          + "daaadddaaaaddaaaaaad\n"
          + "daaaaaaaaadddddaaaad\n"
          + "daaaaaaaaadddgdcaccd\n"
          + "dpaaaaaaaaaadddaaaad\n"
          + "dgaaaaaaaaaaaaaaaaad\n"
          + "dgaaaaraaaaaaaaaaaad\n"
          + "dpaaaaaaaaaaaaaacaad\n"
		  + "daaaaaaaazcaaaaaaaad\n"
		  + "dddddddddddddddddddd\n";

			
	private int w = 0;
    @SuppressWarnings("unused")
	private int h = 0;
    /** Nombre del archivo de musica que se va a usar en el juego (se especifica el directorio)*/
    protected String directorioMusica;
	
	/**
	 * Punto de entrada al juego. Simplemente se crea una instancia
	 * a una clase que inicializa el renderizado y el loop principal
	 * 
	 * @param argv Argumentos del juego
	 */
	public static void main(String argv[]) {
		
        Linguo2 ex = new Mundo();
        ex.gameLoop();
		
	}
	
	public Mundo() {
		super();

		// Setea el Canvas y lo coloca en el Panel del JFrame
		canvas.setBounds(0,0,800,600);
		//panelJuego.add(canvas);

		// Dice a AWT que no se preocupe por repintar el canvas
		// ya que lo haremos nosotros en modo accelerado
		canvas.setIgnoreRepaint(true);

		// Agrega el sistema de input (Clase anidada abajo) al canvas
		// para pdoer responder al input del teclado
		canvas.addKeyListener(new KeyInputHandler());
		
		// pide foco para que el input del teclado venga aca
		canvas.requestFocus();
		// crea el strategy del buffer que permite a AWT
		// manejar graficos accelerados
		canvas.createBufferStrategy(2);
		strategy = canvas.getBufferStrategy();
		
		//Inicia la musica de fondo
		audioSource.startMusic(0);
		//Inicia el nivel
		reiniciarNivel();
		
	}
	
	/**
	 * Reinica el Nivel
	 */
	protected void reiniciarNivel() {
		limpiarPinceladas();
		if (Nivel != null) limpiarNivel();
		initWorld();
		hud = new HUD(robot);	
		hud.setNafta("nafta.png", 80, 5);
		hud.setBalas("polvora.png", 160, 5);
		hud.setPuntaje("gold.png", 80, 40);
		hud.setBaul("cono.png", 0, 100);
		toggleHUD = true;
		// nulifica los valores de las teclas para evitar errores
		upPressed = false;
		leftPressed = false;
		rightPressed = false;
		firePressed = false;
		changeRobot = false;
		brazoPressed = false;
		toggleHelp = false;		
	}
	
	public void resetRobot() {
		reiniciarNivel();
	}
	
	public void cargarMapa(String levelData) {
		this.levelData = levelData;
		reiniciarNivel();
	}
	
	/**
	 *  Limpia la Matriz del Nivel
	 */
	public void limpiarNivel() {
		for (int i=0;i<Nivel.length;i++) {
			for(int j=0;j<Nivel[i].length;j++) {
				Nivel[i][j] = null;
			}
		}
	}
	
	/**
	 *  Limpia las pinceladas del Nivel
	 */
	public void limpiarPinceladas() {
		pinceladas.clear();
	}
	
	/**
	 * Inicializa la Matriz del Nivel.
	 * Lee los datos del nivel y los usa para cargar la Matriz
	 */
	public void initWorld() {
        
        int x = OFFSET;
        int y = OFFSET;
        
        Nivel = new Piso[15][20];
        Piso piso;
        
        if(tipoRobot == "Civil"){
			robot = new Robot(this, null, "robot.png", x, y, "hudRobot.png");
		}else{
			robot = new Militar(this, null, "militar.png", x, y, "hudMilitar.png");
		}

        //Fila de la matriz del Nivel
        int j = 0;
        int i=0;
        
        while(i<levelData.length()){
      
        	int indiceMatriz=0;
            char item = levelData.charAt(i);
        	while(item!='\n'){
        	
        		switch(item) {
        		
        		//PISOS BASE
        			case 'a':
        				piso = new Piso(robot, "arena.png", x, y, "arena");
        				Nivel[j][indiceMatriz] = piso;
        				x += SPACE;
        				break;
        			case 's':
        				piso = new Piso(robot, "asfalto.png", x, y, "asfalto");
        				Nivel[j][indiceMatriz] = piso;
        				x += SPACE;
        				break;
        			case 'g':
        				piso = new Piso(robot, "agua.png", x, y, "agua");
        				Nivel[j][indiceMatriz] = piso;
        				x += SPACE;
        				break;
        			case 'f':
        				piso = new Piso(robot, "fuego.png", x, y, "fuego");
        				Nivel[j][indiceMatriz] = piso;
        				x += SPACE;
        				break;
        			case 'd':
        				piso = new Piso(robot, "arena.png", x, y, "arena", "concreto");
        				Nivel[j][indiceMatriz] = piso;
        				x += SPACE;
        				break;
        			case '#':
        				piso = new Piso(robot, "pared.png", x, y, "pared");
        				Nivel[j][indiceMatriz] = piso;
        				x += SPACE;
        				break;
        				
        			//OBJETOS SOBRE ARENA
        			case 'o':
        				piso = new Piso(robot, "arena.png", x, y, "objetivo");
        				pisoObjetivo = piso;
        				Nivel[j][indiceMatriz] = piso;
        				x += SPACE;
        				break;
        			case 'p':
        				piso = new Piso(robot, "arena.png", x, y, "arena", "planta");
        				Nivel[j][indiceMatriz] = piso;
        				x += SPACE;
        				break;
        			case 'c':
        				piso = new Piso(robot, "arena.png", x, y, "arena", "caja");
        				Nivel[j][indiceMatriz] = piso;
        				x += SPACE;
        				break;
        			case 'r':
        				piso = new Piso(robot, "arena.png", x, y, "arena");
       					piso.setRobot(true);
       					robot.setPisoActual(piso);
       					robot.setX(x);
       					robot.setY(y);
        				Nivel[j][indiceMatriz] = piso;
        				robotPosFil = j;
        				robotPosCol = indiceMatriz;
        				x += SPACE;
        				break;
        			case 'n':
        				piso = new Piso(robot, "arena.png", x, y, "arena", "nafta");
        				Nivel[j][indiceMatriz] = piso;
        				x += SPACE;
        				break;
        			case 'b':
        				piso = new Piso(robot, "arena.png", x, y, "arena", "balas");
        				Nivel[j][indiceMatriz] = piso;
        				x += SPACE;
        				break;
        			case 't':
        				piso = new Piso(robot, "arena.png", x, y, "arena", "puntos");
        				Nivel[j][indiceMatriz] = piso;
        				x += SPACE;
        				break;
        			case 'z':
        				piso = new Piso(robot, "arena.png", x, y, "arena", "baliza");
        				Nivel[j][indiceMatriz] = piso;
        				x += SPACE;
        				break;
        			case 'i':
        				piso = new Piso(robot, "arena.png", x, y, "arena", "cono");
        				Nivel[j][indiceMatriz] = piso;
        				x += SPACE;
        				break;
        			
        			//OBJETOS SOBRE ASFALTO
        			case 'O':
        				piso = new Piso(robot, "asfalto.png", x, y, "objetivo");
        				pisoObjetivo = piso;
        				Nivel[j][indiceMatriz] = piso;
        				x += SPACE;
        				break;
        			case 'P':
        				piso = new Piso(robot, "asfalto.png", x, y, "asfalto", "planta");
        				Nivel[j][indiceMatriz] = piso;
        				x += SPACE;
        				break;
        			case 'C':
        				piso = new Piso(robot, "asfalto.png", x, y, "asfalto", "caja");
        				Nivel[j][indiceMatriz] = piso;
        				x += SPACE;
        				break;
        			case 'R':
        				piso = new Piso(robot, "asfalto.png", x, y, "asfalto");
       					piso.setRobot(true);
       					robot.setPisoActual(piso);
       					robot.setX(x);
       					robot.setY(y);
        				Nivel[j][indiceMatriz] = piso;
        				robotPosFil = j;
        				robotPosCol = indiceMatriz;
        				x += SPACE;
        				break;
        			case 'N':
        				piso = new Piso(robot, "asfalto.png", x, y, "asfalto", "nafta");
        				Nivel[j][indiceMatriz] = piso;
        				x += SPACE;
        				break;
        			case 'B':
        				piso = new Piso(robot, "asfalto.png", x, y, "asfalto", "balas");
        				Nivel[j][indiceMatriz] = piso;
        				x += SPACE;
        				break;
        			case 'T':
        				piso = new Piso(robot, "asfalto.png", x, y, "asfalto", "puntos");
        				Nivel[j][indiceMatriz] = piso;
        				x += SPACE;
        				break;
        			case 'Z':
        				piso = new Piso(robot, "asfalto.png", x, y, "asfalto", "baliza");
        				Nivel[j][indiceMatriz] = piso;
        				x += SPACE;
        				break;
        			case 'I':
        				piso = new Piso(robot, "asfalto.png", x, y, "asfalto", "cono");
        				Nivel[j][indiceMatriz] = piso;
        				x += SPACE;
        				break;	
            
        		}
        		h = y;
        		indiceMatriz++;
        		i++;
        		item = levelData.charAt(i);
        		}
            y += SPACE;
            if (this.w < x) 
            	this.w = x;
            x = OFFSET;
            j++; //Bajo de linea
            i++;
            
        }
    }
	
	/**
	 * El gran ciclo infinito. Este loop corre durante toda la 
	 * ejecuccion del juego.
	 * <p>
	 * - Maneja la velocidad del ciclo para actualizar los movimientos
	 * - Mueve las entidades
	 * - Dibuja los graficos
	 * - Actualiza los eventos
	 * - Comprueba el input
	 * <p>
	 */
	public void gameLoop() {
		long lastLoopTime = System.currentTimeMillis();
		
		//Ciclar hasta el fin del mundo
		while (gameRunning) {
			// Calcular cuanto tiempo transcurrio desde el ultimo Update.
			// Se utiliza para saber cuento hay que mover a las entidades
			
			long delta = System.currentTimeMillis() - lastLoopTime;
			lastLoopTime = System.currentTimeMillis();

			// actualiza el contador de frames (cuadros)
			lastFpsTime += delta;
			fps++;
			
			// actualiza el FPS (cuadros por segundo) si un segundo a transcurrido
			if (lastFpsTime >= 1000) {
				container.setTitle(windowTitle+" (FPS: "+fps+")"+currentScriptPath);
				lastFpsTime = 0;
				fps = 0;
			}
			
			// Obtener el contexto Grafico y limpiarlo
			Graphics2D g = (Graphics2D) strategy.getDrawGraphics();
			g.setRenderingHints(rh);

			g.setColor(Color.black);
			g.fillRect(0,0,canvas.getWidth(),canvas.getHeight());
			
			// save the original transform so that we can restore
		    // it later
		    AffineTransform saveTransform = g.getTransform();

		    // blank the screen. If we do not call super.paintComponent, then
		    // we need to blank it ourselves
		    //ourGraphics.setColor(Color.WHITE);
		    //ourGraphics.fillRect(0, 0, getWidth(), getHeight());
				
		    // We need to add new transforms to the existing
		    // transform, rather than creating a new transform from scratch.
		    // If we create a transform from scratch, we will
		    // will start from the upper left of a JFrame, 
		    // rather than from the upper left of our component
		    at = new AffineTransform(saveTransform);

		    // The zooming transformation. Notice that it will be performed
		    // after the panning transformation, zooming the panned scene,
		    // rather than the original scene
		    at.translate(canvas.getWidth()/2, canvas.getHeight()/2);
		    at.scale(scale, scale);
		    at.translate(-canvas.getWidth()/2, -canvas.getHeight()/2);

		    // The panning transformation
		    at.translate(translateX, translateY);

		    g.setTransform(at);
			
            
			// recorrer la lista de balas instanciadas y pedirles que se muevan
			if (!waitingForKeyPress) {
				for (int i=0;i<balas.size();i++) {
					Bala disparo = (Bala) balas.get(i);
					
					disparo.move(delta);
				}
			}
			
			//Animar el Robot al Moverse
			if (robot.moviendose()) {
				robot.move(delta,directionMove);
			}
			
			if (!waitingForKeyPress) {
				// Recorre la Matriz del Nivel dibujando las Entidades.
				for (int i=0;i<Nivel.length;i++) {
					for(int j=0;j<Nivel[i].length;j++) {
						Entidad entity = Nivel[i][j];
						if (entity!=null){
							entity.draw(g);
						}
					}
				}
				
				//Si la grilla esta activada la dibujamos
				if(showGrid) {
					int a = 0; int b = 0;
					for (int i=0;i<Nivel.length;i++) {
						for(int j=0;j<Nivel[i].length;j++) {
							g.drawRect (40, 40, a, b); 
							a += 40;
						}
						b += 40;
						a = 0;
					}
				}
				
				// Recorre la coleccion de Disparos para dibujarlos
				for (int i=0;i<pinceladas.size();i++) {
					Pincelada pincelada = pinceladas.get(i);
					pincelada.draw(g);
				}
				// Recorre la coleccion de Disparos para dibujarlos
				for (int i=0;i<balas.size();i++) {
					Bala disparo = balas.get(i);
					
					disparo.draw(g);
				}
				//Dibujamos al Robot
				if (!robot.girando())
					robot.draw(g);
				else
					robot.moveGirar(delta, g, direccionGiro);
			}
			
			// Fuerza bruta sobre las colisiones, compara cada Disparo Activo
			// Con todas los objetos que esten en la direccion a la que esta mirando el Robot.
			// Si se encuentra un impacto, se notifica al Disparo
			int k = 0;
			boolean impacto = false;
			//Recorre la lista de balas
			while(k<balas.size() && !impacto){
				Bala me = balas.get(k++);
				//int orientacion=robot.getOrientacion();
				//int fila=robotPosFil;
				//int col=robotPosCol;
				
				for(int i = 0; i < Nivel.length;i++){
					for(int j = 0; j < Nivel[i].length;j++){
						Objeto target=Nivel[i][j].getObjeto();
						if(target!=null){
							impacto=me.collidesWith(target);
							if(impacto){
								me.collidedWith(target);
							}
						}
					}
				}

			}
			
			// remueve cualquier bala que haya sido marcada para elimnar
			balas.removeAll(balasBasura);
			balasBasura.clear();

			
			// Si estamos esperando el ingreso de una tecla
			if (waitingForKeyPress) {

				g.setColor(Color.WHITE);
				/*
				if(startUp) {
					g.setFont( new Font( "SansSerif", Font.BOLD, 72 ) );
				
					g.drawString("LINGUO 2 Prototype",(800-g.getFontMetrics().stringWidth("LINGUO 2 Prototype"))/2,200);
					String subtitle = "The Empire Strike Back on the Lost Ark of the Temple of Doom, in a New Hope";
					g.setFont( new Font( "SansSerif", Font.BOLD, 20 ) );
					g.drawString(subtitle,(800-g.getFontMetrics().stringWidth(subtitle))/2,240);
					g.drawString("Creditos",(800-g.getFontMetrics().stringWidth("Creditos"))/2,400);
					g.setFont( new Font( "SansSerif", Font.BOLD, 30 ) );
					g.drawString("Ignacio del Barrio",(800-g.getFontMetrics().stringWidth("Ignacio del Barrio"))/2,440);
					g.drawString("Ignacio Jocano",(800-g.getFontMetrics().stringWidth("Ignacio Jocano"))/2,480);
					g.drawString("Alexis Aguilera",(800-g.getFontMetrics().stringWidth("Alexis Aguilera"))/2,520);
				}
				*/
				g.setFont( new Font( "SansSerif", Font.BOLD, 36 ) );
				g.drawString(message,(800-g.getFontMetrics().stringWidth(message))/2,250);
				g.drawString("PRESIONE UNA TECLA",(800-g.getFontMetrics().stringWidth("PRESIONE UNA TECLA"))/2,300);
			}
			
			if (!waitingForKeyPress) {
				//Redibujamos el Heads Up Display
				if(toggleHUD) hud.draw(g);

			}
			
			// terminamos de dibujar. Tiramos los graficos
			g.setTransform(saveTransform);
			g.dispose();
			strategy.show();
			
			
			//Movimiento del robot
			
			if ((leftPressed) && (!rightPressed) && (!upPressed) && (!downPressed) && (!robot.moviendose()) && (!robot.girando())) {
				girarRobot(-1);
			} else if ((rightPressed) && (!leftPressed) && (!upPressed) && (!downPressed) && (!robot.moviendose()) && (!robot.girando())) {
				girarRobot(1);
			} else if ((!rightPressed) && (!leftPressed) && (upPressed) && (!downPressed) && (!robot.girando()) && (!robot.moviendose())) {
				moverRobot(1);
			} else if ((!rightPressed) && (!leftPressed) && (!upPressed) && (downPressed) && (!robot.girando()) && (!robot.moviendose())) {
				moverRobot(-1);
			}
			
			if (firePressed) {
				intentarAccionEspecial();
			}
			
			if (brazoPressed) {
				desplegarBrazoRobot();
			}
			
			//Cambiar de robot
			if (changeRobot) {
				toggleRobot();
			}
			
			// Comprobar si se cumplio el Objetivo del DEMO
			if(robot.getPisoActual() == pisoObjetivo){
				notificarNivelCompletado();
			}
				
			// Para que cada cuadro dure 10 ms, guardamos en una variable lastLoopTime
			// cuando commenzamos el cuadro. Agregamos 10 ms a esto y 
			// le restamos el tiempo actual. Esto nos da el tiempo que 
			// debemos esperar hasta el siguiente cuadro.
			// A razon de 10 ms por cuadro tendriamos 100 FPS clavados.
			try { Thread.sleep(lastLoopTime+10-System.currentTimeMillis()); } catch (Exception e) {} //20
		}
	}
	
	/**
	 * Intenta cambiar el tipo de Robot
	 * Solo puede realizar una accion cada X ms.
	 * Esto evita movimientos extremadamente rapidos.
	 */
	public void toggleRobot() {
		// comprobar que esperamos el tiempo nesesario
		if (System.currentTimeMillis() - ultimaAccion < intervaloDeAccion) {
			return;
		}
		
		ultimaAccion = System.currentTimeMillis();

		if(tipoRobot == "Militar") {
			tipoRobot = "Civil";
		}else{
			tipoRobot = "Militar";
		}
		reiniciarNivel();
	}
	
	/**
	 * Intentar Girar el Robot.
	 * Solo puede realizar una accion cada X ms.
	 * Esto evita movimientos extremadamente rapidos.
	 */
	public void girarRobot(int direccion) {
		// comprobar que esperamos el tiempo nesesario
		if (System.currentTimeMillis() - ultimaAccion < intervaloDeAccion) {
			return;
		}
		
		ultimaAccion = System.currentTimeMillis();
		
		direccionGiro = direccion;
		
		robot.girar(direccion);
		if(robot.getNafta() <= 0)
			notificarNaftaAcabada();
	}
	
	/**
	 * Avanza el Robot al Piso que tiene en frente, de ser transitable.
	 * Solo puede realizar una accion cada X ms.
	 * Esto evita movimientos extremadamente rapidos.
	 */
	public void moverRobot(int direccion) {
		// comprobar que esperamos el tiempo nesesario
		if (System.currentTimeMillis() - ultimaAccion < intervaloDeAccion) {
			return;
		}
		ultimaAccion = System.currentTimeMillis();
		
		directionMove=direccion;
		
		Piso origen = Nivel[robotPosFil][robotPosCol];
		Piso destino = null;
		int orientacion = robot.getOrientacion();
		
		if(direccion > 0){
			switch (orientacion) {
			case 0://NORTE
				{
					destino = Nivel[robotPosFil-1][robotPosCol];
					if(robot.avanzar(origen, destino)) robotPosFil--;
				}break;
			case 1://ESTE
				{
					destino = Nivel[robotPosFil][robotPosCol+1];
					if(robot.avanzar(origen, destino)) robotPosCol++;
				}break;
			case 2://SUR
				{
					destino = Nivel[robotPosFil+1][robotPosCol];
					if(robot.avanzar(origen, destino)) robotPosFil++;
				}break;
			case 3://OESTE
				{
					destino = Nivel[robotPosFil][robotPosCol-1];
					if (robot.avanzar(origen, destino))	robotPosCol--;
				}break;
			}
		}else if(direccion < 0){
			switch (orientacion) {
			case 2://SUR
				{
					destino = Nivel[robotPosFil-1][robotPosCol];
					if(robot.avanzar(origen, destino)) robotPosFil--;
				}break;
			case 3://OESTE
				{
					destino = Nivel[robotPosFil][robotPosCol+1];
					if(robot.avanzar(origen, destino)) robotPosCol++;
				}break;
			case 0://NORTE
				{
					destino = Nivel[robotPosFil+1][robotPosCol];
					if(robot.avanzar(origen, destino)) robotPosFil++;
				}break;
			case 1://ESTE
				{
					destino = Nivel[robotPosFil][robotPosCol-1];
					if (robot.avanzar(origen, destino))	robotPosCol--;
				}break;
			}
		}
		if(robot.puedePintar()){
			boolean existe = false;
			for(Pincelada x : pinceladas){
				existe = existe || x.pertenece(origen, destino);
			}
			if (!existe)
				pinceladas.add(new Pincelada(origen, destino, direccion, orientacion, robot.getColor()));
		}
		if(robot.getNafta() <= 0)
			notificarNaftaAcabada();
	
	}
	
	/**
	 * Pide al Robot que cambie el color del Pincel
	 * Tambien recorre las pinceladas en pantalla y les cambia el color.
	 * 
	 * @param c Color
	 */
	public void setPincelColor(Color c) {
		robot.setColor(c);
		for(Pincelada x : pinceladas) {
			x.setColor(c);
		}
	}
	
	/**
	 * Retorna el color del pincel del robot
	 * 
	 * @return Color pincel
	 */
	public Color getPincelColor() {
		return robot.getColor();
	}
	
	/**
	 * Activa el Modo Pintar
	 * 
	 */
	public void activarModoPintar() {
		robot.desplegarPincel();
	}
	
	/**
	 * Desactiva el Modo Pintar
	 * 
	 */
	public void desactivarModoPintar() {
		robot.guardarPincel();
	}
	
	/**
	 * Despliega el Brazo del Robot para agarrar el Objeto que tenga enfrente.
	 * Solo puede realizar una accion cada X ms.
	 * Esto evita movimientos extremadamente rapidos.
	 */
	public void desplegarBrazoRobot() {
		// comprobar que esperamos el tiempo nesesario
		if (System.currentTimeMillis() - ultimaAccion < intervaloDeAccion) {
			return;
		}
		ultimaAccion = System.currentTimeMillis();
				
		switch (robot.getOrientacion()) {
		case 0://NORTE
			{
				Piso destino = Nivel[robotPosFil-1][robotPosCol];
				robot.desplegarBrazo(destino);
			}break;
		case 1://ESTE
			{
				Piso destino = Nivel[robotPosFil][robotPosCol+1];
				robot.desplegarBrazo(destino);
			}break;
		case 2://SUR
			{
				Piso destino = Nivel[robotPosFil+1][robotPosCol];
				robot.desplegarBrazo(destino);
			}break;
		case 3://OESTE
			{
				Piso destino = Nivel[robotPosFil][robotPosCol-1];
				robot.desplegarBrazo(destino);
			}break;
		}
		
	}
	
	public void addInstanciaBala(Bala disparo) {
		balas.add(disparo);
	}
	
	/**
	 * Intenta utilizar la Accion Especial del Robot
	 * Solo puede realizar una accion cada X ms.
	 * Esto evita movimientos extremadamente rapidos.
	 */
	public void intentarAccionEspecial() {
		// comprobar que esperamos el tiempo nesesario
		if (System.currentTimeMillis() - ultimaAccion < intervaloDeAccion) {
			return;
		}
		ultimaAccion = System.currentTimeMillis();
				
		robot.hazTuGracia();
	}
	
	/**
	 * Notificacion de alguna Entidad de que la logica del juego deberia ser
	 * ejecutada en el siguiente ciclo.
	 */
	//public void updateLogic() {
		//logicRequiredThisLoop = true;
	//}
	
	/**
	 * Elimina una entidad del juego
	 * 
	 * @param entity La entidad a ser removida
	 */
	public void eliminarDisparo(Bala disparo) {
		balasBasura.add(disparo);
	}
	
	/**
	 * Notifica al Destructible del daño recibido
	 * 
	 * @param entity The entity that should be removed
	 */
	public void dañarDestructible(Destructible dest) {
		dest.recibirDaño();
	}
	
	/**
	 * Notificacion de que el jugador a perdido
	 */
	public void notificarNaftaAcabada() {
		//Esperamos un segundo para no cortar el juego repentinamente
		//try {Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}
		message = "TE HAS QUEDADO SIN COMBUSTIBLE!";
		upPressed = false;
		leftPressed = false;
		rightPressed = false;
		firePressed = false;
		changeRobot = false;
		brazoPressed = false;
		toggleHelp = false;	
		waitingForKeyPress = true;
	}
	
	/**
	 * Notificacion de que el jugador a cumplido el objetivo
	 */
	public void notificarNivelCompletado() {
		//Esperamos un segundo para no cortar el juego repentinamente
		//try {Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}
		message = "HAS LLEGADO AL OBJETIVO!";
		upPressed = false;
		leftPressed = false;
		rightPressed = false;
		firePressed = false;
		changeRobot = false;
		brazoPressed = false;
		toggleHelp = false;	
		waitingForKeyPress = true;
	}
	
	/**
	 * A class to handle keyboard input from the user. The class
	 * handles both dynamic input during game play, i.e. left/right 
	 * and shoot, and more static type input (i.e. press any key to
	 * continue)
	 * 
	 * This has been implemented as an inner class more through 
	 * habbit then anything else. Its perfectly normal to implement
	 * this as seperate class if slight less convienient.
	 * 
	 * @author Kevin Glass
	 */
	private class KeyInputHandler extends KeyAdapter {
		/** The number of key presses we've had while waiting for an "any key" press */
		private int pressCount = 1;
		
		/**
		 * Notification from AWT that a key has been pressed. Note that
		 * a key being pressed is equal to being pushed down but *NOT*
		 * released. Thats where keyTyped() comes in.
		 *
		 * @param e The details of the key that was pressed 
		 */
		public void keyPressed(KeyEvent e) {
			// if we're waiting for an "any key" typed then we don't 
			// want to do anything with just a "press"
			if (waitingForKeyPress) {
				return;
			}
			
			
			if (e.getKeyCode() == KeyEvent.VK_UP) {
				upPressed = true;
			}
			if (e.getKeyCode() == KeyEvent.VK_DOWN) {
				downPressed = true;
			}
			if (e.getKeyCode() == KeyEvent.VK_LEFT) {
				leftPressed = true;
			}
			if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
				rightPressed = true;
			}
			if (e.getKeyCode() == KeyEvent.VK_SPACE) {
				firePressed = true;
			}
			//Toggle HUD con H
			if (e.getKeyCode() == KeyEvent.VK_H) {
				toggleHUD = !toggleHUD;
			}
			//Toggle Robot con T
			if (e.getKeyCode() == KeyEvent.VK_T) {
				changeRobot = true;
			}
			//Toggle Brazo con E
			if (e.getKeyCode() == KeyEvent.VK_E) {
				brazoPressed = true;
			}
			//Ayuda con F1
			if (e.getKeyCode() == KeyEvent.VK_F1) {
				toggleHelp = !toggleHelp;
			}
		} 
		
		/**
		 * Notification from AWT that a key has been released.
		 *
		 * @param e The details of the key that was released 
		 */
		public void keyReleased(KeyEvent e) {
			// if we're waiting for an "any key" typed then we don't 
			// want to do anything with just a "released"
			if (waitingForKeyPress) {
				return;
			}
			
			if (e.getKeyCode() == KeyEvent.VK_UP) {
				upPressed = false;
			}
			if (e.getKeyCode() == KeyEvent.VK_DOWN) {
				downPressed = false;
			}
			if (e.getKeyCode() == KeyEvent.VK_LEFT) {
				leftPressed = false;
			}
			if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
				rightPressed = false;
			}
			if (e.getKeyCode() == KeyEvent.VK_SPACE) {
				firePressed = false;
			}
			//Reinciciar Nivel con R
			if (e.getKeyCode() == KeyEvent.VK_R) {
				reiniciarNivel();
			}
			//Toggle Robot con T
			if (e.getKeyCode() == KeyEvent.VK_T) {
				changeRobot = false;
			}
			//Toggle Brazo con E
			if (e.getKeyCode() == KeyEvent.VK_E) {
				brazoPressed = false;
			}
			
		}

		/**
		 * Notification from AWT that a key has been typed. Note that
		 * typing a key means to both press and then release it.
		 *
		 * @param e The details of the key that was typed. 
		 */
		public void keyTyped(KeyEvent e) {
			// if we're waiting for a "any key" type then
			// check if we've recieved any recently. We may
			// have had a keyType() event from the user releasing
			// the shoot or move keys, hence the use of the "pressCount"
			// counter.
			if (waitingForKeyPress) {
				if (pressCount == 1) {
					// since we've now recieved our key typed
					// event we can mark it as such and start 
					// our new game
					waitingForKeyPress = false;
					startUp = false;
					reiniciarNivel();
					pressCount = 0;
				} else {
					pressCount++;
				}
			}	
			// if we hit escape, then quit the game
			if (e.getKeyChar() == KeyEvent.VK_ESCAPE) {
				System.exit(0);
			}
		}
	}
}
