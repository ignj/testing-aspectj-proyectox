package Logica.Entidades;

import java.awt.Color;
import java.awt.Graphics;

import Logica.Textures.Sprite;
import Logica.Textures.SpriteManager;


/**
 * Entidad que representa un Piso del juego.
 * Los Pisos pueden ser, o no, transitables por el Robot. Dependiendo del tipo.
 * El Piso tambien guarda informacion sobre el Objeto que tiene sobre el.
 * 
 * Existe un tipo especial de Piso el cual es "Objetivo del Juego".
 * Este tipo de Piso es especial para la Entrega 2.
 * 
 * @author Ignacio del Barrio, Ignacio Jocano, Alexis Aguilera
 * 
 */
public class Piso extends Entidad{
	
	/** Verdadero si el Robot puede desplazarse sobre el */
	protected boolean transitable;
	/** Tipo del Piso (Arena, Asfalto, Objetivo) */
	protected String tipo;
	/** Color del que esta pintado el Piso */
	protected Color color;
	/** Objeto actual en el Piso */
	protected Objeto objeto;
	/** Referencia al Robot. */
	protected Robot robot;
	/** Sprite del Objetivo */
	protected Sprite objetivo;
	/** Verdadero si el Robot se encuentra en este piso */
	protected boolean estaRobot;
	
	/** 
	 * Constructor por defecto
	 */
	public Piso(Robot r, String ref, int x,int y, String tipo) {
		super(ref, x, y);
		objetivo = SpriteManager.get().getSprite("objetivo.png");
		this.tipo = tipo;
		transitable=(tipo=="arena" || tipo=="asfalto" || tipo=="objetivo");
		color = null;
		robot = r;
		estaRobot = false;
	}
	
	/** 
	 * Constructor Ampliado.
	 * Se utiliza para rapidamente crear Pisos con Objetos en el.
	 */
	public Piso(Robot r, String ref, int x,int y, String tipo, String tipoObjeto) {
		super(ref, x, y);
		objetivo = SpriteManager.get().getSprite("objetivo.png");
		this.tipo = tipo;
		robot = r;
		estaRobot = false;
		switch (tipoObjeto) {
			case "concreto" : objeto = new Objeto(this, "concreto.png", x, y, "concreto");
			break;
			case "caja" : objeto = new Destructible(this, "caja.png", x, y, "caja", 2);
			break;
			case "planta" : objeto = new Destructible(this, "planta.png", x, y, "planta", 1);
			break;
			case "cono" : objeto = new Coleccionable(this, "cono.png", x, y, "cono");
			break;
			
			case "nafta":
				objeto = new Premio(this, "nafta.png", x, y, "nafta", 10);
				break;
			case "balas":
				objeto = new Premio(this, "polvora.png", x, y, "balas", 10);
				break;
			case "puntos":
				objeto = new Premio(this, "gold.png", x, y, "puntos", 10);
				break;
				
		}
		transitable=(tipo=="arena" || tipo=="asfalto");
		color = null;
	}
	
	/**
	 * Nafta requerida para moverse a este piso
	 * 
	 * @param Nafta Requerida
	 */
	public int naftaRequerida() {
		int toret = 0;
		switch (tipo) {
			case "arena" : toret=2;
			break;
			case "asfalto" : toret=1;
			break;
		}
		return toret;
	}
	
	/** 
	 * Retorna si el Piso es transitable
	 * 
	 * @returns transitable Verdadero si es transitable
	 */
	public boolean esTransitable() {
		return transitable;
	}
	
	/** 
	 * Retorna el tipo del Piso
	 * 
	 * @returns tipo Tipo del Piso
	 */
	public String getTipo() {
		return tipo;
	}
	
	/** 
	 * Actualiza la referencia al Objeto en el Piso
	 * 
	 * @param o Objeto
	 */
	public void setObjeto(Objeto o) {
		objeto = o;
	}

	/** 
	 * Especifica si el Robot se encuentra en este Piso
	 * 
	 */
	public void setRobot( boolean vf ) {
		estaRobot = vf;
	}
	
	/**
	 * Redefinicion del metodo Draw para poder Dibujar tambien los objetos en el Piso.
	 * 
	 * @param g El contexto Grafico en el cual dibujar
	 */
	public void draw(Graphics g) {
		sprite.draw(g,(int) x,(int) y);
		if(tipo == "objetivo"){
			objetivo.draw(g, (int) x,(int) y);
		}
		if (objeto != null){
			objeto.draw(g);
		}/*
		if (robot != null){
			robot.draw(g);
		}*/
	}

	/** 
	 * Retorna el Objeto actual que esta en este Piso
	 * 
	 * @returns objeto Objeto en este Piso
	 */
	public Objeto getObjeto() {
		return objeto;
	}
	
	/** 
	 * Retorna si el Robot esta o no en este Piso
	 * 
	 * @returns verdadero si el Robot esta en este Piso
	 */
	public boolean hasRobot() {
		return estaRobot;
	}
}
