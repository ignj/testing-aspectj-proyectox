package Logica.Entidades;

import java.awt.Graphics;
import java.awt.Rectangle;

import Logica.Textures.Sprite;
import Logica.Textures.SpriteManager;

/** 
 * Una Entidad factoriza las caracteristicas y comportamiento compartido por todos los
 * objetos que nesesitan de una representacion grafica.
 * Informacion sobre el Sprite que lo representa asi como las coordenadas en pantalla y
 * metodos de dibujado. 
 * 
 * @author Ignacio del Barrio, Ignacio Jocano, Alexis Aguilera
 */
public abstract class Entidad {
	/** Posicion actual en x */  
	protected double x;
	/** Posicion actual en y */
	protected double y;
	/** El Sprite que representa a esta Entidad */
	protected Sprite sprite;
	/** La velocidada horizontal actual de la entidad (pixels/sec) */
	protected double dx;
	/** La velocidad vertical actual de la entidad (pixels/sec) */
	protected double dy;
	/** Rectangulo utilizado por otras entidad para manejar Colisiones */
	protected Rectangle me = new Rectangle();
	/** La velocidad a la que se mueve la entidad */
	protected double moveSpeed;
	
	/**
	 * Construye una Entidad en base a la ubicacion de su imagen y coordenadas.
	 * 
	 * @param ref Referencia a la imagen a ser utilizada en esta entidad
 	 * @param x La posicion inicial en x
	 * @param y La posicion inicial en y
	 */
	public Entidad(String ref,int x,int y) {
		//Llama a la unica instancia del Manejador de Sprites y le pide busque la imagen ref
		this.sprite = SpriteManager.get().getSprite(ref);
		this.x = x;
		this.y = y;
		me.setBounds((int) x,(int) y,sprite.getWidth(),sprite.getHeight());
	}
	
	/**
	 * Actualiza la posicion horizontal
	 * 
	 * @param x Posicion Horizontal
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * Actualiza la posicion vertical
	 * 
	 * @param y Posicion Vertical
	 */
	public void setY(int y) {
		this.y = y;
	}
	
	/**
	 * Actualiza la velocidad Horizontal
	 * 
	 * @param dx Velocidad Horizontal
	 */
	public void setHorizontalMovement(double dx) {
		this.dx = dx;
	}

	/**
	 * Actualiza la velocidad Vertical
	 * 
	 * @param dx Velocidad Vertical
	 */
	public void setVerticalMovement(double dy) {
		this.dy = dy;
	}
	
	/**
	 * Retorna la velocidad Horizontal
	 * 
	 * @return La velocidad Horizontal
	 */
	public double getHorizontalMovement() {
		return dx;
	}

	/**
	 * Retorna la velocidad Vertical
	 * 
	 * @return La velocidad Vertical
	 */
	public double getVerticalMovement() {
		return dy;
	}
	
	/**
	 * Dibuja esta entidad en el contexto Grafico otorgado
	 * 
	 * @param g El contexto Grafico en el cual dibujar
	 */
	public void draw(Graphics g) {
		sprite.draw(g,(int) x,(int) y);
	}
	
	/**
	 * Logica asociada a esta entidad
	 * Este metodo se llama periodicamente desde el Mundo
	 */
	//public void doLogic() {}
	
	/**
	 * Retorna la posicion Horizontal
	 * 
	 * @return Posicion Horizontal
	 */
	public int getX() {
		return (int) x;
	}

	/**
	 * Retorna la posicion Vertical
	 * 
	 * @return Posicion Vertical
	 */
	public int getY() {
		return (int) y;
	}
	
	/**
	 * Retorna un rectangulo con los limites de la entidad.
	 * 
	 * @return Limites de la entidad
	 */
	public Rectangle getBounds(){
		return me;
	}
	
	/**
	 * Retorna la velocidad con la que se mueve el proyectil
	 * 
	 * @return moveSpeed Velocidad del proyectil
	 */
	public double getMoveSpeed() {
		return moveSpeed;
	}
	
	/**
	 * Actualiza la velocidad del proyectil
	 * 
	 * @param speed Velocidad del proyectil
	 */
	public void setMoveSpeed(double speed) {
		moveSpeed = speed;
	}
}
