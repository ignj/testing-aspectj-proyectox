package Logica.Entidades;

/**
 * Entidad que repesenta un Objeto Coleccionable del juego.
 * 
 * @author Ignacio del Barrio, Ignacio Jocano, Alexis Aguilera
 */
public class Coleccionable extends Objeto {
	
	public Coleccionable(Piso p, String ref,int x,int y, String tipo) {
		super(p, ref, x, y, tipo);
	
	}

	/**
	 * Agrega un Objeto Coleccionable al baul del Robot
	 * 
	 */
	public void dispararEvento(Robot robot) {
		robot.addColeccionable(this);
		piso.setObjeto(null);
	}
}
